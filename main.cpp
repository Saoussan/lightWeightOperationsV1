#include "lightWeightOperations.h"
#include <iostream>
#include <string>
#include <string.h>
#include <fstream>
#include <vector>
#include <stdio.h>
#include "matcreuse.h"
using namespace std;

int main()
{

int choix;
int n,x,y;
cout<<"\t\tProgramme de calcul matriciel elementaire:\n\n\n";
cout<<"Menu:\n\nPour l'addition taper: 0\nPour la soustraction taper 1\nPour la division par un entier taper 2\nPour la multiplication par un entier sans chargement en mémoire taper 3\n"<<endl;
cin>>choix;
if(choix==0)
{
    //Matcreuse A = Matcreuse();
    Matcreuse A = Matcreuse();
    Matcreuse B = Matcreuse();
    A.Add(B);
    A.Afficher();
}
if(choix==1)
{
    Matcreuse A = Matcreuse();
    Matcreuse B = Matcreuse();

    A.Sub(B);
    A.Afficher();
}
if(choix==2)
{
  do{
  cout<<"Entrer votre entier:"<<endl;
  cin>>n;
    }while(n==0);

    Matcreuse A = Matcreuse();
    A.Div_int(n);
    A.Afficher();
}

if(choix==3)
{
lightWeightOperations lwo;
string inputFile;
inputFile="LCV.txt";
string outputFile;
outputFile="RETURNLCV.txt";
lwo.multiplyByInteger(2,inputFile,outputFile);
}


return 0;
}
