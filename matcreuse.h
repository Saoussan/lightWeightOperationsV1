#include<string>
using namespace std;

class Matcreuse{

		int n_Lig;
		int n_Col;
		int n_CasesR; //Nombre de cases remplies
		int ** mat;
		int val_case;

		public:
			Matcreuse();	   //constructeur par défaut
			Matcreuse(int,int);//constructeur avec deux arguments (nb lignes et col)
			~Matcreuse();      //destructeur de la classe Matcreuse
			Matcreuse(Matcreuse&,bool); // !!!!!
			Matcreuse(Matcreuse&,Matcreuse&); // !!!!!

			void Afficher(); //affichage d'une matrice
			void Initialiser(); //initialisation de Matrice

			int n_Lg();
			int n_Cl();
			int ** Matc();

			void Add(Matcreuse &);// Additionne 2 Matrices X et Y dans X
			void Sub(Matcreuse &);// Soustraction de 2 Matrices
			void Div_int(int);    // Divise une Matrice par un Entier
};


