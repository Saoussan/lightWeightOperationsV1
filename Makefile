librairie : main.o lightWeightOperations.o matcreuse.o
	g++ main.o lightWeightOperations.o matcreuse.o -o librairie -pg
 
main.o : main.cpp lightWeightOperations.h matcreuse.h
	g++ -c main.cpp -o main.o -pg
 
lightWeightOperations.o : lightWeightOperations.cpp lightWeightOperations.h
	g++ -c lightWeightOperations.cpp -o lightWeightOperations.o -pg
matcreuse.o : matcreuse.cpp matcreuse.h
	g++ -c matcreuse.cpp -o matcreuse.o -pg

clean:
	rm -rf *.o
